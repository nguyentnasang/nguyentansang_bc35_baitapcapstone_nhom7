import {
  duyetNoiLuuTru,
  NumberGioHang,
  rendercart,
  renderDanhSachSanPham,
  saveLocalStore,
  tinhTongGiaSp,
} from "./controllor.js";
let cart = [];
let sumPrice = "";

// 3. Hiển thị danh sách sản phẩm cho khách hàng.
axios({
  url: `https://633ec0610dbc3309f3bc5528.mockapi.io/ProductList`,
  method: "GET",
})
  .then(function (res) {
    renderDanhSachSanPham(res.data);
  })
  .catch(function (err) {
    console.log("err", err);
  });
//   4. Tạo một ô select cho phép người dùng filter theo loại sản phẩm

axios({
  url: `https://633ec0610dbc3309f3bc5528.mockapi.io/ProductList`,
  method: "GET",
})
  .then(function (res) {
    document.getElementById("searchSelect").addEventListener("click", () => {
      let valueSel = document.getElementById("selecProduct").value;
      if (valueSel == 0) {
        renderDanhSachSanPham(res.data);
        duyetNoiLuuTru(res.data, cart, sumPrice);
      } else if (valueSel == 1) {
        let iphone = [];
        res.data.forEach((Element) => {
          if (Element.type == "Iphone") {
            iphone.push(Element);
          }
          renderDanhSachSanPham(iphone);
          duyetNoiLuuTru(iphone, cart, sumPrice);
        });
      } else if (valueSel == 2) {
        let SamSung = [];
        res.data.forEach((Element) => {
          if (Element.type == "SamSung") {
            SamSung.push(Element);
          }
          renderDanhSachSanPham(SamSung);
          duyetNoiLuuTru(SamSung, cart, sumPrice);
        });
      }
    });
  })

  .catch(function (err) {
    console.log("err", err);
  });
//thêm sản phẩm vào giỏ hàng

let dataLocal = localStorage.getItem("DSSP");
if (dataLocal) {
  cart = JSON.parse(dataLocal);
  rendercart(cart);
}
axios({
  url: `https://633ec0610dbc3309f3bc5528.mockapi.io/ProductList`,
  method: "GET",
})
  .then((res) => {
    duyetNoiLuuTru(res.data, cart, sumPrice);
  })
  .catch((err) => {
    console.log("err", err);
  });
//xóa sản phẩm khỏi giỏ hàng

let xoaSp = (idsp) => {
  let index = cart.findIndex((sp) => {
    return sp.id == idsp;
  });
  ///////////////
  cart.splice(index, 1);
  rendercart(cart);
  saveLocalStore(cart);
  let TotalSum = 0;

  let price = document.querySelectorAll(".priceSp");
  price.forEach((sp) => {
    TotalSum += sp.innerHTML * 1;
  });
  sumPrice = TotalSum;
  document.getElementById("totalPrice").innerHTML = TotalSum;
  NumberGioHang(cart);
};
window.xoaSp = xoaSp;
// tang quantify
let upValue = (idsp) => {
  var index = cart.findIndex((sp) => {
    return sp.id == idsp;
  });
  // cart[index].quantity = upqtf;
  if (cart[index].quantity <= 9) {
    cart[index].quantity += 1;
  } else {
    Swal.fire("Bạn chỉ có thể mua 10 mặt hàng cho mỗi sản phẩm");
  }
  rendercart(cart);
  let TotalSumUp = 0;

  let price = document.querySelectorAll(".priceSp");
  price.forEach((sp) => {
    TotalSumUp += sp.innerHTML * 1;
  });
  sumPrice = TotalSumUp;

  document.getElementById("totalPrice").innerHTML = TotalSumUp;
  saveLocalStore(cart);
  // saveLocalStore(sumTotal());
};
window.upValue = upValue;
// giảm quantify
let downValue = (idsp) => {
  var index = cart.findIndex((sp) => {
    return sp.id == idsp;
  });
  cart[index].quantity -= 1;
  if (cart[index].quantity == -1) {
    cart.splice(index, 1);
  }

  rendercart(cart);
  let TotalSum = 0;

  let price = document.querySelectorAll(".priceSp");
  price.forEach((sp) => {
    TotalSum += sp.innerHTML * 1;
  });
  sumPrice = TotalSum;
  document.getElementById("totalPrice").innerHTML = TotalSum;
};
window.downValue = downValue;
window.onload = function () {
  let TotalSum = 0;

  let price = document.querySelectorAll(".priceSp");
  price.forEach((sp) => {
    TotalSum += sp.innerHTML * 1;
  });
  sumPrice = TotalSum;
  document.getElementById("totalPrice").innerHTML = TotalSum;
};
//giỏ hàng
///click hiện giỏ
document.querySelector(".fa-shopping-cart").addEventListener("click", () => {
  document.querySelector(".cart").style.display = "block";
});
// click ẩn giỏ
document.querySelector(".fa-times").addEventListener("click", () => {
  document.querySelector(".cart").style.display = "none";
});
document.getElementById("pay").addEventListener("click", () => {
  let ktpt = document.getElementById("totalPrice").innerHTML;
  if (ktpt == 0) {
    Swal.fire("Quý khách hãy thêm sản phẩm trước khi thanh toán");
  }
});

// thanh toán fix khi tải lại trang
document.getElementById("pay").addEventListener("click", () => {
  let ktpt = document.getElementById("totalPrice").innerHTML;
  if (ktpt != 0) {
    cart = [];
    saveLocalStore(cart);
    rendercart(cart);
    tinhTongGiaSp();

    Swal.fire("THANH TOÁN THÀNH CÔNG");
    NumberGioHang(cart);
  }
});
// dọn dẹp clear giỏ fix khi tải lại trang
document.getElementById("clear_cart").addEventListener("click", () => {
  cart = [];

  saveLocalStore(cart);
  rendercart(cart);
  tinhTongGiaSp();
  Swal.fire("DỌN DẸP GIỎ HÀNG THÀNH CÔNG");
  NumberGioHang(cart);
});
