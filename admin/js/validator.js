export let validatorForm = (value, error) => {
    if(value.length === 0) {
        document.getElementById(error).innerHTML = 'Vui lòng nhập trường này';
        return false;
    } else {
        document.getElementById(error).innerHTML = '';
        return true;
    };
};