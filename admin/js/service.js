let URL_API = 'https://633ec0610dbc3309f3bc5528.mockapi.io';

// Get data from API
export let getProduct = () => {
    return axios({
        url: `${URL_API}/ProductList`,
        method: 'GET',
    });
};

// Post data to API
export let postProduct = product => {
    return axios({
        url: `${URL_API}/ProductList`,
        method: 'POST',
        data: product
    });
};

// Delete data from API
export let deleteProduct = id => {
    return axios({
        url: `${URL_API}/ProductList/${id}`,
        method: 'DELETE',
    });
};

// Get product to API
export let getProductUpdate = (id) => {
    return axios({
        url: `${URL_API}/ProductList/${id}`,
        method: 'GET',
    });
};

// Submit product revisions to API
export let updateProduct = (product, id) => {
    return axios({
        url: `${URL_API}/ProductList/${id}`,
        method: 'PUT',
        data: product
    });
};