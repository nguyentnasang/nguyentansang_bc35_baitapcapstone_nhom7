// Function get value from
export let getValueForm = () => {
    const id = document.getElementById('id').value;
    const type = document.getElementById('type').value;
    const price = document.getElementById('price').value;
    const screen = document.getElementById('screen').value;
    const name = document.getElementById('name').value;
    const frontCamera = document.getElementById('frontCamera').value;
    const backCamera = document.getElementById('backCamera').value;
    const img = document.getElementById('image').value;
    const desc = document.getElementById('description').value;

    return {
        id,
        type,
        price,
        screen,
        name,
        frontCamera,
        backCamera,
        img,
        desc,
    };
};

// Function render list product
export let renderListProduct = list => {
    let contentHTML = '';

    list.forEach(item => {
        contentHTML += `
            <tr>
                <td>${item.name}</td>
                <td>${item.type}</td>
                <td>$ ${item.price}</td>
                <td>
                    <img src="${item.img}" style="height: auto; width: 70px" alt="${item.name}" />
                </td>
                <td>${item.desc}</td>
                <td>
                    <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" onclick="getIdProduct(${item.id})">Sủa</button>
                    <button class="btn btn-danger" onclick="deleteItemProduct(${item.id})">Xóa</button>
                </td>
            </tr>
        `;
    });
    document.getElementById('tbody-table').innerHTML = contentHTML;
};

// Function show data product to form
export let showInfoInForm = (product) => {
    document.getElementById('id').value = product.id;
    document.getElementById('price').value = product.price;
    document.getElementById('screen').value = product.screen;
    document.getElementById('name').value = product.name;
    document.getElementById('frontCamera').value = product.frontCamera;
    document.getElementById('backCamera').value = product.backCamera;
    document.getElementById('image').value = product.img;
    document.getElementById('description').value = product.desc;
};

// Function close modal add product
export let closeModalResetForm = () => {
    let addProduct = document.getElementById('addProduct');
    addProduct.setAttribute('data-dismiss', 'modal');
    document.getElementById('form-block').reset();
    document.getElementById('exampleModal').style.display = 'none';
};

// Function close modal update product
export let closeModalResetFormUpdate = () => {
    let updateProduct = document.getElementById('updateProduct');
    updateProduct.setAttribute('data-dismiss', 'modal');
    document.getElementById('form-block').reset();
    document.getElementById('exampleModal').style.display = 'none';
};

// Function toggle button get id product
export let toggleBtnGetIdProduct = () => {
    document.getElementById('updateProduct').style.display = 'block';
    document.getElementById('form-id').style.display = 'block';
    document.getElementById('id').disabled = true;
    document.getElementById('addProduct').style.display = 'none';
};

// Function toggle button add new product
export let toggleBtnAddNewProduct = () => {
    document.getElementById('updateProduct').style.display = 'none';
    document.getElementById('addProduct').style.display = 'block';
    document.getElementById('form-id').style.display = 'none';
    document.getElementById('form-block').reset();
};