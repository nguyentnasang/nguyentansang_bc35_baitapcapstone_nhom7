import { 
    closeModalResetForm, 
    closeModalResetFormUpdate, 
    getValueForm, 
    renderListProduct, 
    showInfoInForm, 
    toggleBtnAddNewProduct, 
    toggleBtnGetIdProduct 
} from './controller.js';
import Product from './modal.js';
import { postProduct, getProduct, deleteProduct, getProductUpdate, updateProduct } from './service.js';
import { validatorForm } from './validator.js';


// Function add product
document.getElementById('addProduct').addEventListener('click', (e) => {
    const dataForm = getValueForm();

    let validate = true;
    validate &= validatorForm(dataForm.type, 'err-type');
    validate &= validatorForm(dataForm.name, 'err-name');
    validate &= validatorForm(dataForm.price, 'err-price');
    validate &= validatorForm(dataForm.screen, 'err-screen');
    validate &= validatorForm(dataForm.frontCamera, 'err-front');
    validate &= validatorForm(dataForm.backCamera, 'err-back');
    validate &= validatorForm(dataForm.backCamera, 'err-image');
    validate &= validatorForm(dataForm.backCamera, 'err-desc');

    if(validate) {
        closeModalResetForm();
        postProduct(dataForm)
        .then((res) => {
            Swal.fire(
                'Thành công',
                'Thêm sản phẩm thành công',
                'success'
            );
            renderProduct();
        })
        .catch((err) => {
            Swal.fire(
                'Thất bại',
                'Thêm sản phẩm thất bại',
                'error'
            );
        });
    };
});

// Function show product 
const renderProduct = () => {
    getProduct()
        .then(res => {
            let listProduct = res.data.map(item => {
                return new Product (
                    item.id,
                    item.name,
                    item.price,
                    item.screen,
                    item.backCamera,
                    item.frontCamera,
                    item.img,
                    item.desc,
                    item.type,
                );
            });
            renderListProduct(listProduct);
        })
        .catch(err => {
            Swal.fire(
                'Thất bại',
                'Hiển thị sản phẩm thất bại',
                'error'
            );
        });
};
renderProduct();

// Function delete product
const deleteItemProduct =(id) => {
    deleteProduct(id)
        .then(res => {
            Swal.fire(
                'Thành công',
                'Xóa sản phẩm thành công',
                'success'
            )
            renderProduct();
        })
        .catch(err => {
            Swal.fire(
                'Thất bại',
                'Xóa sản phẩm thất bại',
                'error'
            );
        });
};
window.deleteItemProduct = deleteItemProduct;

// Function get data product from api 
const getIdProduct = (id) => {
    getProductUpdate(id)
        .then(res => {
            var product = res.data;
            showInfoInForm(product);
            toggleBtnGetIdProduct();
        })
        .catch(err => {
            Swal.fire(
                'Thất bại',
                'Lấy thông tin sản phẩm thất bại',
                'error'
            );
        });
};
window.getIdProduct = getIdProduct;

// Function update product
document.getElementById('updateProduct').addEventListener('click', () => {
    let getForm = getValueForm();

    let validate = true;
    validate &= validatorForm(getForm.type, 'err-type');
    validate &= validatorForm(getForm.name, 'err-name');
    validate &= validatorForm(getForm.price, 'err-price');
    validate &= validatorForm(getForm.screen, 'err-screen');
    validate &= validatorForm(getForm.frontCamera, 'err-front');
    validate &= validatorForm(getForm.backCamera, 'err-back');
    validate &= validatorForm(getForm.backCamera, 'err-image');
    validate &= validatorForm(getForm.backCamera, 'err-desc');

    if(validate) {
        closeModalResetFormUpdate();
        updateProduct(getForm, getForm.id)
        .then(res => {
            Swal.fire(
                'Thành công',
                'Cập nhật sản phẩm thành công',
                'success'
            );
            renderProduct();
            document.getElementById('id').disabled = false;
        })
        .catch(err => {
            Swal.fire(
                'Thất bại',
                'Cập nhật sản phẩm thất bại',
                'error'
            );
        });
    };
});

// Function add new product
document.getElementById('addNew').addEventListener('click', () => {
    toggleBtnAddNewProduct();
});