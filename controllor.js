// thêm số vào giỏ hàng
export let NumberGioHang = (listCart) => {
  let totalSp = document.getElementById("totalSp");
  let totalNum = 0;
  listCart.forEach((Element) => {
    totalNum += Element.quantity;
  });
  totalSp.innerHTML = totalNum;
};

export let renderDanhSachSanPham = (list) => {
  let contentHTML = "";
  list.forEach((element) => {
    contentHTML += `
    <div class='product'>
    <div class="list_img">
    <img src="${element.img}" alt="" />
    </div>
    <div class="product_name">${element.id}-${element.name}</div>
    <div>${element.type}</div>
    <div class="mota">
    <span>Mô tả sản phẩm: ${element.desc}</span>,
    <span>màn hình: ${element.screen}</span>,
    <span>camera trước: ${element.frontCamera}</span>,
    <span>camera sau: ${element.backCamera}</span>.</div>
    <div>
    ${element.price}VNĐ
    <button class="btn btn-danger product_item">Thêm vào giỏ hàng</button>
    </div>
    </div>

  `;
  });
  document.getElementById("ProductList").innerHTML = contentHTML;
};
//////////////////////////////////////////////
export let rendercart = (list) => {
  let contentTbody = "";
  list.forEach((element) => {
    contentTbody += `<tr>
    <td class="item_img">
    <img src="${element.img}" alt="" />
    </td>
    <td>${element.name}</td>
    <td>
    <button onclick="downValue(${
      element.id
    })" class="btn_left"><i class="fas fa-chevron-left"></i></button>
    ${element.quantity}
    <button onclick="upValue(${
      element.id
    })" class="btn_right"><i class="fas fa-chevron-right"></i></button>

    </td>
    <td class='priceSp' >${element.price * element.quantity}</td>
    <td>
    <button onclick='xoaSp(${element.id})' value='${
      element.id
    }' class='btn btn-danger'>Xóa</button>
    </td>
    </tr>`;
  });
  document.getElementById("tbodyProduct").innerHTML = contentTbody;
};
//save LocalStore
export let saveLocalStore = (list) => {
  let dataJson = JSON.stringify(list);
  localStorage.setItem("DSSP", dataJson);
};
// tạo lại  của từng region
export let duyetNoiLuuTru = (dataPro, listCart, sum) => {
  NumberGioHang(listCart);
  let list_item = document.querySelectorAll(".product_item");
  list_item.forEach((Element, index) => {
    Element.addEventListener("click", () => {
      dataPro[index].quantity = 1;

      let itemId = [];
      listCart.forEach((element) => {
        itemId.push(element.id);
      });
      let checkId = itemId.includes(dataPro[index].id);

      if (checkId == false) {
        Swal.fire("Thêm thành công");
        listCart.push(dataPro[index]);
        ///////////////////////////////
        NumberGioHang(listCart);
        //////////////////////////////////
      } else if (checkId == true) {
        Swal.fire("sản phẩm này đã có trong giỏ hàng");
      }

      //////////////////
      rendercart(listCart);

      let TotalSum = 0;

      let price = document.querySelectorAll(".priceSp");
      price.forEach((sp) => {
        TotalSum += sp.innerHTML * 1;
      });
      sum = TotalSum;
      document.getElementById("totalPrice").innerHTML = TotalSum;
      saveLocalStore(listCart);
      /// clear giỏ
      document.getElementById("clear_cart").addEventListener("click", () => {
        listCart = [];

        saveLocalStore(listCart);
        rendercart(listCart);
        tinhTongGiaSp();
        Swal.fire("DỌN DẸP GIỎ HÀNG THÀNH CÔNG");
        NumberGioHang(listCart);
      });
      /// thanh toán
      document.getElementById("pay").addEventListener("click", () => {
        let ktpt = document.getElementById("totalPrice").innerHTML;
        if (ktpt != 0) {
          listCart = [];
          saveLocalStore(listCart);
          rendercart(listCart);
          tinhTongGiaSp();

          Swal.fire("THANH TOÁN THÀNH CÔNG");
          NumberGioHang(listCart);
        }
      });
    });
  });
};
///////////////////////
export let tinhTongGiaSp = () => {
  let TotalSum = 0;

  let price = document.querySelectorAll(".priceSp");
  price.forEach((sp) => {
    TotalSum += sp.innerHTML * 1;
  });

  document.getElementById("totalPrice").innerHTML = TotalSum;
};
